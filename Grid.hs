module Grid where

import qualified Data.Bifunctor as DB
import qualified Data.IntMap.Strict as Map
import qualified Data.List.Split as DLS

-- TODO: Try having the map empty and returning a Maybe on atPosition (does Map already have ability to return a Maybe?)
-- Then we can also just store position as keys and could have an "infinite" grid
-- Replace ncol in fromList with width
-- Can we lift maybe? and throw error if not possible?
-- day14 (and 12) would be a good performance checker. With day 14 I wouldn't need to xoffset anymore, downside is we would
-- would have to guess appropiate floor size or have the fallingSand take into account the floors position

data Grid a = Grid
  { ncol :: Int,
    nrow :: Int,
    grid :: Map.IntMap a,
    def :: a
  }

-- | x (cols) and y (rows) position
type Position = (Int, Int)

-- | Create a Grid from a list (row by row)
fromList :: Int -> [a] -> Grid a
fromList ncol xs = grd
  where
    grd =
      Grid
        { ncol = ncol,
          nrow = length xs `div` ncol,
          grid = Map.fromList $ zip [0 ..] xs,
          def = head xs
        }

-- | Create a Grid from a list of lists (inner lists are seen as row)
fromLists :: [[a]] -> Grid a
fromLists xss = grd
  where
    grd =
      Grid
        { ncol = length $ head xss,
          nrow = length xss,
          grid = Map.fromList $ zip [0 ..] $ concat xss,
          def = head $ head xss
        }

-- | Returns a list with the filled positions
toList :: Grid a -> [(Position, a)]
toList grd = Prelude.map (DB.first (toPosition grd)) (Map.toAscList (grid grd))

const :: Int -> Int -> a -> Grid a
-- const ncol nrow value = fromList ncol $ replicate (ncol * nrow) value
const ncol nrow value =
  Grid
    { ncol = ncol,
      nrow = nrow,
      grid = Map.empty,
      def = value
    }

-- map :: (a -> b) -> Grid a -> Grid b
-- map f grd = grd {grid = Map.map f mp}
--   where
--     mp = grid grd
--
atPosition :: Grid a -> Position -> a
atPosition grd pos
  | Map.member i (grid grd) = grid grd Map.! i
  | otherwise = def grd
  where
    i = fromPosition grd pos

setPosition :: Position -> a -> Grid a -> Grid a
setPosition pos value grd = newGrid
  where
    newGrid =
      grd
        { Grid.grid =
            Map.insert (fromPosition grd pos) value (Grid.grid grd)
        }

toPosition :: Grid a -> Int -> Position
toPosition grid i = (mod i $ ncol grid, i `div` ncol grid)

fromPosition :: Grid a -> Position -> Int
fromPosition grid (x, y) = y * ncol grid + x

validPosition :: Grid a -> Position -> Bool
validPosition grd (x, y) =
  x >= 0 && x < ncol grd
    && y >= 0
    && y < nrow grd

filter :: (a -> Bool) -> Grid a -> [(Position, a)]
filter f grd = Prelude.map (DB.first (toPosition grd)) $ Map.assocs mp
  where
    mp = Map.filter f $ Grid.grid grd

-- Node that here 0, 0 is the top, with x being the column and y going down the rows
-- print grd = mapM_ Prelude.print $ toLists grd
