module Main where

import Common (extractNumbers2, nubOrd)
import Data.List (foldl', sortBy)
import Data.Maybe (catMaybes)
import qualified Grid
import Test.Tasty
import Test.Tasty.HUnit

type Beacon = ((Int, Int), (Int, Int))

-- TODO: Should really limit this to one dimensional intervals (x only in this case, since y is the same)
type Interval = ((Int, Int), (Int, Int))

readLine str = ((x0, y0), (x1, y1))
  where
    [x0, y0, x1, y1] = extractNumbers2 str

manhattan (x0, y0) (x1, y1) = abs (x0 - x1) + abs (y0 - y1)

-- TODO: Should only get the to and from
scanned :: Int -> Beacon -> Maybe Interval
scanned y (b, c)
  | dd > d = Nothing
  | otherwise = Just xs
  where
    d = manhattan b c
    (bx, by) = b
    (cx, cy) = c
    dd = manhattan (bx, by) (bx, y)
    df = abs $ d - dd
    xs = ((bx - df, y), (bx + df, y))

getFromX = fst . fst

getToX iv = fst $ snd iv

maxX (x0, y0) (x1, y1)
  | x0 < x1 = (x1, y1)
  | otherwise = (x0, y0)

concatIntervals :: [Maybe Interval] -> [Interval]
concatIntervals xs = go [] ivs
  where
    ivs = sortBy (\a b -> compare (getFromX a) (getFromX b)) $ catMaybes xs
    go xs [] = reverse xs
    go [] (y : ys) = go [y] ys
    go (x : xs) (y : ys)
      | 1 + getToX x >= getFromX y = go ((fst x, maxX (snd x) (snd y)) : xs) ys
      | otherwise = go (y : x : xs) ys

lInterval iv = getToX iv - getFromX iv

getRows contents = map go
  where
    beacons = map readLine $ lines contents
    go y = concatIntervals $ map (scanned y) beacons

--getRow beacons y = nubOrd $ concatMap (scanned y) beacons

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day15.txt", "sample_day15_2.txt", "input_day15.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "functions" $ do
        extractNumbers2 "x=-2, y=100032" @?= [-2, 100032],
      testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let beacons = map readLine $ lines contents
        let y = 10
        let ivs = map (scanned y) beacons
        let ivs' = concatIntervals ivs
        let l = foldl' (\acc iv -> acc + lInterval iv) 0 ivs'
        l @?= 26,
      testCase "FullTest" $
        do
          contents <- readFile $ last myFiles
          let beacons = map readLine $ lines contents
          let y = 2000000
          let ivs = map (scanned y) beacons
          let ivs' = concatIntervals ivs
          let l = foldl' (\acc iv -> acc + lInterval iv) 0 ivs'
          l @?= 5181556,
      testCase
        "SampleTestPart2"
        $ do
          contents <- readFile $ myFiles !! 1
          let xmax = 20
          let row = head $ filter (\r -> length r > 1) $ getRows contents [0 .. xmax]
          let (x, y) = snd $ head row
          (x+1)*4000000 + y @?= 56000011,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let xmax = 4000000
        let row = head $ filter (\r -> length r > 1) $ getRows contents [0 .. xmax]
        let (x, y) = snd $ head row
        (x+1)*xmax + y @?= 12817603219131
    ]
