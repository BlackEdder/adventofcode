module Main where

import qualified Data.IntMap.Strict as IMap
import Data.List (foldl', iterate', scanl')
import Data.List.Split (chunksOf)
import qualified Grid
import Test.Tasty
import Test.Tasty.HUnit

data Block = Min | Plus | Corner | Long | Square

data Space = Empty | Rock

type Direction = (Int, Int)

-- Position is left bottom
asPos :: Grid.Position -> Block -> [Grid.Position]
asPos (x, y) Min = [(x, y), (x + 3, y), (x + 1, y), (x + 2, y)]
--asPos (x, y) Plus = [(x, y + 1), (x + 1, y), (x + 1, y + 1), (x + 1, y + 2), (x + 2, y + 1)]
asPos (x, y) Plus = [(x, y + 1), (x + 2, y + 1), (x + 1, y), (x + 1, y + 2) ]
asPos (x, y) Corner = [(x, y), (x + 2, y), (x + 1, y), (x + 2, y + 2), (x + 2, y + 1)]
asPos (x, y) Long = [(x, y), (x, y + 3), (x, y + 1), (x, y + 2)]
asPos (x, y) Square = [(x, y), (x + 1, y), (x, y + 1), (x + 1, y + 1)]

heightBlock Min = 1
heightBlock Plus = 3
heightBlock Corner = 3
heightBlock Long = 4
heightBlock Square = 2

blockWidth Min = 4
blockWidth Plus = 3
blockWidth Corner = 3
blockWidth Long = 1
blockWidth Square = 2

move :: Grid.Position -> Direction -> Grid.Position
move (x, y) (a, b) = (x + a, y + b)

isEmpty Empty = True
isEmpty Rock = False

emptyAndValidSpace grd pos
  | not (Grid.validPosition grd pos) = False
  | otherwise = isEmpty $ Grid.atPosition grd pos

canMove :: Grid.Grid Space -> Block -> Grid.Position -> Bool
canMove grd blk pos = all (emptyAndValidSpace grd) $ asPos pos blk

addToGrid grd blk pos = foldl' (\acc x -> Grid.setPosition x Rock acc) grd locs
  where
    locs = asPos pos blk

dropBlock' grd blk pos dir = (cont, pos'')
  where
    ppos = move pos dir
    -- jet of air
    pos' = if canMove grd blk ppos then ppos else pos
    ppos' = move pos' (0, -1)
    cont = canMove grd blk ppos'
    pos'' = if cont then ppos' else pos'

convertJet '<' = (-1, 0)
convertJet x = (1, 0)

data State = State
  { grid :: Grid.Grid Space,
    blocks :: [Block],
    jets :: [Direction],
    tHeight :: Int
  }

slideGrid :: Grid.Grid Space -> Int -> Grid.Grid Space
slideGrid grd dy = grd' {Grid.grid = mp}
  where
    grd' = Grid.const (Grid.ncol grd) (Grid.nrow grd) Empty
    di = dy * Grid.ncol grd
    (_, mp) = IMap.split (-1) $ IMap.mapKeysMonotonic (\i -> i - di) (Grid.grid grd)

purgeGrid :: Grid.Grid Space -> Int -> Grid.Grid Space
purgeGrid grd dy = grd {Grid.grid = mp}
  where
    di = dy * Grid.ncol grd
    (_, mp) = IMap.split di (Grid.grid grd)

totalHeight = 2*1000000000000

dropBlock state = go state (True, (2, tHeight state + 3))
  where
    headBlock:tailBlocks = blocks state
    go state (False, pos) =
      seq grd'' $ seq tHeight' $ seq tailBlocks
        ( state
            { blocks = tailBlocks,
              tHeight = tHeight',
              grid = grd'',
            },
          pos
        )
      where
        h = snd pos + heightBlock headBlock
        tHeight' = if h > tHeight state then h else tHeight state
        grd' = addToGrid (grid state) headBlock pos
        check = tHeight' `mod` 300 == 290
        grd'' = if check then purgeGrid grd' (tHeight' - 100) else grd'
    go state (_, pos) = seq js $ go (state {jets = js}) x
      where
        hj:js = jets state
        x = dropBlock' (grid state) headBlock pos hj

finalScore :: State -> Double 
finalScore state = fromIntegral $ tHeight state

dropBy m x = if null m' then [last m] else m'
    where
        m' = drop x m

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day17.txt", "sample_day17_2.txt", "input_day17.txt"]
tests =
  testGroup
    "Tests"
    [ testCase "functions" $ do
        1 @?= 1,
      testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let jets' = cycle $ map convertJet $ head $ lines contents
        let blocks = cycle [Min, Plus, Corner, Long, Square]
        let towerHeight = 0
        let grd = Grid.const 7 totalHeight Empty
        -- The cycle
        let state = State {grid = grd, blocks = blocks, jets = jets', tHeight = towerHeight}
        let step = iterate' (\(s, _) -> dropBlock s) (state, (0, 0))
        snd (step !! 1) @?= (2, 0)
        snd (step !! 2) @?= (2, 1)
        snd (step !! 3) @?= (0, 3)
        print $ tHeight $ fst (step !! 3)
        snd (step !! 4) @?= (4, 3)
        snd (step !! 5) @?= (4, 7)
        let (finalS, finalP) = step !! 2022
        tHeight finalS @?= 3068,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let jets' = cycle $ map convertJet $ head $ lines contents
        let blocks = cycle [Min, Plus, Corner, Long, Square]
        let towerHeight = 0
        let grd = Grid.const 7 totalHeight Empty
        -- The cycle
        let state = State {grid = grd, blocks = blocks, jets = jets', tHeight = towerHeight}
        let nsteps = 2022
        let (finalS, finalP) = foldl' (\(s, _) i -> dropBlock s) (state, (0, 0)) [0 .. nsteps]
        tHeight finalS @?= 3235,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let nsteps' = 1000000000000
        let nsteps = 1000000
        print $ nsteps' `div` nsteps
        let jets' = cycle $ map convertJet $ head $ lines contents
        let blocks = cycle [Min, Plus, Corner, Long, Square]
        let towerHeight = 0
        let grd = Grid.const 7 totalHeight Empty
        -- The cycle
        let state = State {grid = grd, blocks = blocks, jets = jets', tHeight = towerHeight}
        let step = scanl' (\(s, _) i -> dropBlock s) (state, (0, 0)) [0..nsteps]
        mapM_ (\x -> print $ round $ finalScore (fst $ last x)) $ chunksOf (nsteps `div` 10) step
        1 @?= 2,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let nsteps = 1000000000000
        let jets' = cycle $ map convertJet $ head $ lines contents
        let blocks = cycle [Min, Plus, Corner, Long, Square]
        let towerHeight = 0
        let grd = Grid.const 7 totalHeight Empty
        -- The cycle
        let state = State {grid = grd, blocks = blocks, jets = jets', tHeight = towerHeight}
        let step = iterate' (\(s, _) -> dropBlock s) (state, (0, 0))
        let (finalS, finalP) = step !! nsteps
        -- 1591860467679
        -- 1591860468347
        -- 1591860468659
        -- 1591860469639
        -- 1591860469644
        -- 1591860469656
        -- 1597428288820 
        --let (finalS, finalP) = foldl' (\(s, _) i -> dropBlock s) (state, (0,0)) [0..nsteps]
        tHeight finalS @?= 1514285714288,
      testCase "FullTestEstimate" $ do
        contents <- readFile $ last myFiles
        let nsteps = 1000000000000
        1 @?= 2
        let jets' = cycle $ map convertJet $ head $ lines contents
        let blocks = cycle [Min, Plus, Corner, Long, Square]
        let towerHeight = 0
        let grd = Grid.const 7 totalHeight Empty
        -- The cycle
        let state = State {grid = grd, blocks = blocks, jets = jets', tHeight = towerHeight}
        let step = iterate' (\(s, _) -> dropBlock s) (state, (0, 0))

        let sols = drop 100000 $ map (\((s,_), i) -> finalScore s * nsteps / i) $ zip step [0..nsteps]
        let alpha = 10000.0
        let cutoff = 100000000.0
        let sols' = scanl' (\(m, a, i, mu, n) s -> (1.0/alpha*(s + (alpha - 1)*m), s, if i < cutoff then i + 1 else -1, if i < 10 then m else mu  + (s - mu)/i, n - 1)) (1, 1, 0, 0, nsteps) sols
        let sols'' = iterate' (\m -> dropBy m 1000000) sols'
        let sols''' = map (\((a, b, _, c, d):xs) -> (round a, round b, round c, d)) sols''
        mapM_ (\x -> print x) sols'''
         
        1 @?= 2
    ]
