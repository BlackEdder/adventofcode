module Main where

import Test.Tasty
import Test.Tasty.HUnit

-- This is a file where I am exploring language features

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day00.txt", "sample_day00_2.txt", "input_day00.txt"]

data Monkey = Monkey
  { no :: Int,
    items :: [Int],
    testDiv :: Int,
    targets :: (Int, Int),
    noIns :: Int
  }

-- Implement my own show monkey
instance Show Monkey where
  show (Monkey no _ _ _ ni) = "M" ++ show (no, ni)

-- Below some learnings from http://learnyouahaskell.com/making-our-own-types-and-typeclasses,
newtype Tst = Tst Int deriving (Show)

instance Num Tst where
  (+) (Tst a) (Tst b) = Tst (a + b)
  fromInteger a = Tst (fromInteger a)

class YesNo a where  
    yesno :: a -> Bool  

instance YesNo Int where  
    yesno 0 = False  
    yesno _ = True

-- Implement my own Eq
instance Eq Tst where  
    Tst x == Tst y = x == y  
    _ == _ = False

data Tst2 a = Just2 a | Nothing2 deriving (Show)
-- Now we need to make sure the given data type can be compared (m)
instance (Eq m) => Eq (Tst2 m) where  
    Just2 x == Just2 y = x == y  
    Nothing2 == Nothing2 = True  
    _ == _ = False

tests =
  testGroup
    "Tests"
    [ testCase "NumTest" $ do
        let t = Tst 3
        t + 8 @?= Tst 11
        let t = Tst 3
        t + Tst 8 @?= Tst 11
        yesno (0 :: Int) @?= False
        Just2 2 /= Nothing2 @? "They are equal after all"
        Just2 2 == Just2 2 @? "They are not equal after all"
    ]
