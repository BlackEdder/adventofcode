module Main where

import Common (nubOrd)
import Data.Char (ord)
import Data.List (foldl', iterate')
import qualified Grid
import Test.Tasty
import Test.Tasty.HUnit

type Direction = (Int, Int)

directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]

heightDiff :: Grid.Grid Int -> Grid.Position -> Grid.Position -> Int
heightDiff themap pos1 pos2 = Grid.atPosition themap pos1 - Grid.atPosition themap pos2

--
validSteps themap pos = poss2
  where
    poss = filter (Grid.validPosition themap) $ map (move pos) directions
    poss2 = filter (\x -> heightDiff themap x pos <= 1) poss

move :: Grid.Position -> Direction -> Grid.Position
move (x, y) (a, b) = (x + a, y + b)

parseInput contents = (grd, grdV, posS, posE)
  where
    lns = lines contents
    baseGrd = Grid.fromLists lns
    (posS, _) = head $ Grid.filter (== 'S') baseGrd
    (posE, _) = head $ Grid.filter (== 'E') baseGrd
    grd = Grid.map (\a -> ord a - ord 'a') $ Grid.setPosition posE 'z' $ Grid.setPosition posS 'a' baseGrd
    grdV = Grid.const (Grid.ncol grd) (Grid.nrow grd) False

-- concatMap validSteps lsteps
doStep themap (visitedmap, ss) = (vm, sts)
  where
    steps = nubOrd $ concatMap (validSteps themap) ss
    sts = filter (not . Grid.atPosition visitedmap) steps
    vm = foldl' (\m pos -> Grid.setPosition pos True m) visitedmap sts

arrived :: Grid.Position -> [Grid.Position] -> Bool
arrived pos steps = pos `elem` steps

countUntil ePos n ((_, ps) : xs)
  | null ps = 1000
  | arrived ePos ps = n
  | otherwise = countUntil ePos (n + 1) xs

findLength themap visitedmap ePos pos = countUntil ePos 0 steps
  where
    steps = iterate' (doStep themap) (visitedmap, pos)

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day12.txt", "sample_day12_2.txt", "input_day12.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let (themap, visitedmap, cPos, ePos) = parseInput contents
        let s1 = doStep themap (visitedmap, [ePos])
        let steps = iterate' (doStep themap) (visitedmap, [cPos])
        let l = countUntil ePos 0 steps
        let l2 = findLength themap visitedmap ePos [cPos]
        -- Grid.print themap
        cPos @?= (0, 0)
        ePos @?= (5, 2)
        l @?= 31
        l2 @?= 31,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let (themap, visitedmap, cPos, ePos) = parseInput contents
        let steps = iterate' (doStep themap) (visitedmap, [cPos])
        let l = countUntil ePos 0 steps
        l @?= 534,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let (themap, visitedmap, cPos, ePos) = parseInput contents
        let mp = Grid.grid themap
        let sPos = map fst $ Grid.filter (== 0) themap
        -- Could also start from end and have height == 0 as ending position
        -- This would need adapting of the order in which positions are given to heightDiff
        let l = findLength themap visitedmap ePos sPos
        l @?= 29,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let (themap, visitedmap, cPos, ePos) = parseInput contents
        let mp = Grid.grid themap
        let sPos = map fst $ Grid.filter (== 0) themap
        -- Could also start from end and have height == 0 as ending position
        let l = findLength themap visitedmap ePos sPos
        l @?= 525
    ]
