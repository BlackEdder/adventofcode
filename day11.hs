module Main where

import Common (extractNumbers, mapWIndex, readInt)
import Data.List (foldl', scanl', sort, transpose, iterate')
import Data.List.Split (splitWhen)
import qualified Data.Map as Map
import Data.Sequences (dropEnd)
import Test.Tasty
import Test.Tasty.HUnit

type Operation = (Opvalue, Int -> Int -> Int, Opvalue)

data Opvalue = Old | Value Int deriving (Show)

readOpvalue "old" = Old
readOpvalue str = Value (readInt str)

type Monkeys = (Map.Map Int Monkey)
type Item = Int

mInspect :: Operation -> Int -> Int
mInspect (x, op, y) old = case (x, y) of
  (Old, Old) -> op old old
  (Old, Value x) -> op old x

data Monkey = Monkey
  { no :: Int,
    items :: [Item],
    op :: Operation,
    testDiv :: Int,
    targets :: (Int, Int),
    noIns :: Int
  }

instance Show Monkey where
  show (Monkey no _ _ _ _ ni) = "M" ++ show (no, ni)

divisable :: Int -> Item -> Bool
divisable denom num = mod num denom == 0

turnItem :: (Int -> Int) -> Monkey -> Item -> (Int, Item)
turnItem coping mky itm = (trgt, nItm)
  where
    v = mInspect (op mky) itm
    nItm = coping v
    dec = divisable (testDiv mky) nItm
    (t0, t1) = targets mky
    trgt = if dec then t0 else t1

turnMonkey :: (Int -> Int) -> Int -> Monkeys -> Monkeys
turnMonkey coping i ms = nMs
  where
    m = ms Map.! i
    trgts = map (turnItem coping m) (items m)
    newM = m {items = [], noIns = length (items m) + noIns m}
    newMs = Map.insert i newM ms
    nMs = foldl' (\acc (trgt, itm) -> giveItem (itm, acc) trgt) newMs trgts

giveItem :: (Item, Monkeys) -> Int -> Monkeys
giveItem (itm, ms) i = Map.insert i newM ms
  where
    m = ms Map.! i
    newM = m {items = items m ++ [itm]}

roundM coping ms = turns
  where
    turns = foldl' (flip (turnMonkey coping)) ms [0 .. length ms - 1]

scoreMonkeys :: Monkeys -> [Int]
scoreMonkeys mks = take 2 $ reverse $ sort $ map noIns (Map.elems mks)

parseInput :: String -> Monkeys
parseInput contents = Map.fromList $ zip [0 ..] monkeysLst
  where
    monkeyStrs = splitWhen null $ lines contents
    monkeysLst = map readMonkey monkeyStrs

readMonkey :: [String] -> Monkey
readMonkey lns =
  Monkey
    { no = head $ extractNumbers $ head lns,
      items = extractNumbers $ lns !! 1,
      op = (readOpvalue o1, if o2 == "*" then (*) else (+), readOpvalue o3),
      testDiv = head $ extractNumbers $ lns !! 3,
      targets = (head $ extractNumbers $ lns !! 4, head $ extractNumbers $ lns !! 5),
      noIns = 0
    }
  where
    [o1, o2, o3] = reverse $ take 3 $ reverse $ words $ lns !! 2

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day11.txt", "sample_day11_2.txt", "input_day11.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "FunctionTests" $ do
        mInspect (readOpvalue "old", (*), readOpvalue "2") 6 @?= 12
        mInspect (readOpvalue "old", (+), readOpvalue "2") 6 @?= 8
        mInspect (readOpvalue "old", (*), readOpvalue "old") 6 @?= 6*6 
        divisable 13 2080 @?= True
        divisable 24 12 @?= False
        divisable 2 13 @?= False
        divisable 2 12 @?= True,
      testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let monkeys = parseInput contents
        let current_monkeys = iterate' (roundM (`div` 3)) monkeys !! 20
        let [x1, x2] = scoreMonkeys current_monkeys
        x1 * x2 @?= 10605,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let monkeys = parseInput contents
        let current_monkeys = iterate' (roundM (`div` 3)) monkeys !! 20
        let [x1, x2] = scoreMonkeys current_monkeys
        x1 * x2 @?= 88208,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let monkeys = parseInput contents
        let scale = product $ map testDiv (Map.elems monkeys)
        let current_monkeys = iterate' (roundM (`mod` scale)) monkeys !! 10000
        let [x1, x2] = scoreMonkeys current_monkeys
        x1 * x2 @?= 2713310158,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let monkeys = parseInput contents
        let scale = product $ map testDiv (Map.elems monkeys)
        let current_monkeys = iterate' (roundM (`mod` scale)) monkeys !! 10000
        let [x1, x2] = scoreMonkeys current_monkeys
        x1 * x2 @?= 21115867968
    ]
