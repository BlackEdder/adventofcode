module Common where

import Data.Char (isDigit, ord)
import Data.List.Split (whenElt, split, dropDelims, dropBlanks)
import qualified Data.Set as Set

-- | Only keep the unique values in a list.
-- Similar to Data.List.nub, but should be more performant
nubOrd :: Ord a => [a] -> [a]
nubOrd xs = go Set.empty xs
  where
    go s (x : xs)
      | Set.member x s = go s xs
      | otherwise = x : go (Set.insert x s) xs
    go _ _ = []

mapWIndex :: ((Int, a) -> b) -> [a] -> [b]
mapWIndex f xs = map f z
  where
    z = zip [0 ..] xs

-- | Currently untested
breakAround :: Int -> [a] -> ([a], a, [a])
breakAround i ys = (x, y, z)
  where
    (x, zs) = splitAt i ys
    y = head zs
    z = tail zs

readInt :: String -> Int
readInt = read

readNumber :: String -> (Int, String)
readNumber str | isDigit $ head str = (read no, xs)
               | otherwise = error "Expected a number"
  where
    (no, xs) = span isDigit str

isCapital :: Char -> Bool
isCapital c = ord c >= ord 'A' && ord c <= ord 'Z'

-- TODO: Make it use read number above
extractNumbers :: String -> [Int]
extractNumbers str = map readInt $ split (dropBlanks . dropDelims $ whenElt (not . isDigit)) str

extractLabels :: String -> [String]
extractLabels str = go [] str 
    where go xs [] = reverse xs
          go xs (y:ys) | isCapital y = let (is, js) = span isCapital ys in
                                     go ((y:is):xs) js
                       | otherwise = go xs ys

-- TODO: Make it use read number above
extractNumbers2 :: String -> [Int]
extractNumbers2 str = go [] str 
    where go xs [] = reverse xs
          go xs (y:ys) | y == '-' && isDigit (head ys) = let (is, js) = span isDigit ys in
                                                    go (-readInt is:xs) js
          go xs (y:ys) | isDigit y = let (is, js) = span isDigit ys in
                                     go (readInt (y:is):xs) js
                       | otherwise = go xs ys


