import Data.List.Split

type Range = (Int, Int)

type RangesPair = (Range, Range)

main = do
  contents <- readFile "input_day04.txt"
  let sections = map readLine . lines $ contents
  -- part1 305
  print $ length $ filter fullOverlap sections
  -- part2 811
  print $ length $ filter partialOverlap sections

readLine :: [Char] -> RangesPair
readLine xs
  | length split == 2 = (readRange $ head split, readRange $ last split)
  | otherwise = error $ "Failed readline on " ++ xs
  where
    split = splitOn "," xs

readRange :: String -> Range
readRange xs
  | length split == 2 = (read $ head split, read $ last split)
  | otherwise = error $ "Failed parsing on " ++ xs
  where
    split = splitOn "-" xs

sortedPair :: RangesPair -> RangesPair
sortedPair ((x0, x1), (y0, y1))
  | x1 - x0 >= y1 - y0 = ((x0, x1), (y0, y1))
  | otherwise = ((y0, y1), (x0, x1))

fullOverlap :: RangesPair -> Bool
fullOverlap tr
  | x0 <= y0 && x1 >= y1 = True
  | otherwise = False
  where
    ((x0, x1), (y0, y1)) = sortedPair tr

partialOverlap :: RangesPair -> Bool
partialOverlap tr
  | x0 <= y0 && x1 >= y0 = True
  | x0 <= y1 && x1 >= y1 = True
  | otherwise = False
  where
    ((x0, x1), (y0, y1)) = sortedPair tr
