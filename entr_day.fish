#!/usr/bin/fish

if test (count $argv) -eq 0
 echo "no arguments"
 exit 1
end

set day (printf "day%02i" $argv[1])
set hf (printf "%s.hs" $day)
set ef (printf "%s" $day)
set if (printf "input_%s.txt" $day)
set sf (printf "sample_%s.txt" $day)
set sf2 (printf "sample_%s_2.txt" $day)

if test -e $hf
    echo "File already exists"
else
    echo "Creating files"
    cat skeleton.hs | sed "s/{smp}/$sf/g" | sed "s/{smp2}/$sf2/g" | sed "s/{ipt}/$if/g" > $hf
    touch $if
    touch $sf
    touch $sf2
end

# ls * | entr "ghc -dynamic $hf; ./$ef"
# ls $hf $if $sf $sf2 | entr -- bash -c "ghc -dynamic $hf; ./$ef; echo "
# ls $hf $if $sf $sf2 | entr -- bash -c "runghc $hf"
ls *$day\.* --hide=$day.hi --hide=$day.o | entr -- bash -c "runghc $hf"
