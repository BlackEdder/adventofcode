module Main where

import Common (extractLabels, extractNumbers2)
import Data.List (foldl', iterate', maximum, sortBy, sort)
import qualified Data.Map as Map
import Test.Tasty
import Test.Tasty.HUnit

type Label = String

type Valve = (Label, Int, [Label])

type ValveMap = Map.Map Label Valve

bfsStep :: ValveMap -> ValveMap -> ValveMap
bfsStep vMap vlvs = res
  where
    trgts = foldl' (\acc (_, _, xs) -> acc ++ xs) [] vlvs
    res = foldl' (\prev lbl -> Map.insert lbl (vMap Map.! lbl) prev) Map.empty trgts

flowScore :: ValveMap -> Int -> Label -> Label -> (Int, Int)
flowScore vMap time from trgt = (max 0 (x * (time - ds - 1)), ds)
  where
    ds = distance vMap from trgt
    (_, x, _) = vMap Map.! trgt

distance valveMap to from = n
  where
    from' = getSubMap valveMap [from]
    xs = iterate' (bfsStep valveMap) from'
    n = if from == to then 0 else length $ takeWhile (not . Map.member to) xs

getSubMap vMap = foldl' (\prev lbl -> Map.insert lbl (vMap Map.! lbl) prev) Map.empty

parseLine str = Map.insert nm (nm, flow, lbls)
  where
    flow = head $ extractNumbers2 str
    _ : nm : lbls = extractLabels str

potentialScore :: ValveMap -> Int -> Int
potentialScore vMap time = foldl' (\prev (s, t) -> prev + s*t) 0 zs
    where
        vs = Map.elems vMap
        scores = reverse $ sort $ map (\(_, x, _) -> x) vs
        zs = zip scores $ iterate' (\t -> max 0 (t - 1)) (time - 1)

main = do
  defaultMain tests
  print "Done"

-- Left over targets, lbl1, lbl2, score, time1, time2
type State = (ValveMap, Label, Label, Int, Int, Int)

takeStep :: ValveMap -> State -> [State]
takeStep vMap (tMap, lbl1, lbl2, sc, t1, t2)
  | null tMap || (t1 <= 0 && t2 <= 0) = [(tMap, lbl1, lbl2, sc, t1, t2)]
  | otherwise = map (\(((sc1, dt1), k1),((sc2, dt2), k2))  -> (Map.delete k1 $ Map.delete k2 tMap, k1, k2, sc + sc1 + sc2, t1 - dt1 - 1, t2 - dt2 - 1)) zs
  where
    ds1 = map (flowScore vMap t1 lbl1) (Map.keys tMap)
    ds2 = map (flowScore vMap t2 lbl2) (Map.keys tMap)
    xs1 = zip ds1 (Map.keys tMap)
    xs2 = zip ds2 (Map.keys tMap)
    zs' = concatMap (\x -> map (\y -> (x, y)) xs2) xs1
    zs = if length xs1 == 1 then [(((0, 0), lbl1), head xs2)] else filter (\((_, k1), (_, k2)) -> k1 /= k2) zs' 

takeSteps :: ValveMap -> [State] -> [State]
takeSteps vMap xs = mp'
  where
    mp = concatMap (takeStep vMap) xs
    mp' = take 50000 $ sortBy (\(_, _, _, a, _, _) (_, _, _, b, _, _) -> compare b a) mp

myFiles = ["sample_day16.txt", "sample_day16_2.txt", "input_day16.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let lns = lines contents
        let mp = Map.empty
        let valveMap = foldl' (flip parseLine) mp lns
        let targetMap = Map.filter (\(_, x, _) -> x > 0) valveMap
        --let dMap = Map.fromList $ concatMap (\lbl -> map (\x -> ((lbl, x), distance valveMap lbl x)) (Map.keys valveMap)) (Map.keys valveMap)
        print targetMap 
        2 @?= 1
      --   let start = "AA"
      --   let time = 26 
      --   let scs = map (flowScore valveMap time start) (Map.keys targetMap)
      --   let step = iterate' (takeSteps valveMap) [(targetMap, start, start, 0, time, time)]
      --   let fnl = step !! time 
      --   let x = maximum $ map (\(_, _, _, s, _, _) -> s) fnl
      --   x @?= 1651,
      -- testCase "FullTest" $ do
      --   contents <- readFile $ last myFiles
      --   let lns = lines contents
      --   let mp = Map.empty
      --   let valveMap = foldl' (flip parseLine) mp lns
      --   let targetMap = Map.filter (\(_, x, _) -> x > 0) valveMap
      --   let start = "AA"
      --   let time = 26
      --   let step = iterate' (takeSteps valveMap) [(targetMap, start, start, 0, time, time)]
      --   let fnl = step !! time
      --   let x = maximum $ map (\(_, _, _, s, _, _) -> s) fnl
      --   x @?= 1796,
      -- testCase "SampleTestPart2" $ do
      --   contents <- readFile $ myFiles !! 1
      --   1 @?= 1707,
      -- testCase "FullTestPart2" $ do
      --   contents <- readFile $ last myFiles
      --   -- larget than 1971, 1991 is too low?
      --   1 @?= 1999
    ]
