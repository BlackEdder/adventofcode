import qualified Data.Set as Set

main = do
  contents <- readFile "input_day06.txt"
  let bytes = head $ lines contents
  let msg = takeUntilSOP bytes
  -- part 1: 1816
  print $ length msg
  let msg2 = takeUntilSOM bytes
  -- part 2: 2625
  print $ length msg2

takeUntilSOM :: String -> String
takeUntilSOM = takeUntilMatch 14

takeUntilSOP :: String -> String
takeUntilSOP = takeUntilMatch 4

takeUntilMatch :: Int -> String -> String
takeUntilMatch n str = go [] n str
  where
    go :: String -> Int -> String -> String
    go str n [] = str
    go str n xs
      | isUnique n xs = str ++ take n xs
      | otherwise = go (str ++ [head xs]) n (tail xs)

isUnique n str
  | length str < n = False
  | otherwise = not . hasDup $ take n str

hasDup :: Ord a => [a] -> Bool
hasDup xs = go Set.empty xs where
  go s (x:xs)
   | Set.member x s = True 
   | otherwise      = go (Set.insert x s) xs
  go _ _            = False
