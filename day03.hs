import Control.Monad
import Data.Char (ord)
import Data.List
import System.IO

main = do
  contents <- readFile "input_day03.txt"
  let backpack = map readLine . lines $ contents
  let matches = map compareCompartments backpack
  let scores = map score matches
  -- part 1 7428
  print $ sum scores
  -- part 2 2650
  let chunks = chunk 3 $ lines contents
  let badges = map findBadge chunks
  print $ score badges

readLine :: [Char] -> (String, String)
readLine xs = splitAt n xs
  where
    n = div (length xs) 2

chunk :: Int -> [a] -> [(a, a, a)]
chunk _ [] = []
chunk n xs
  | n > 0 = tuplify3 (take n xs) : chunk n (drop n xs)
  | otherwise = error "Negative or zero n"

tuplify3 :: [a] -> (a, a, a)
tuplify3 [x, y, z] = (x, y, z)

findBadge :: (String, String, String) -> Char
findBadge (xs, ys, zs) = head $ intersect zs $ intersect xs ys

score :: String -> Int
score xs = sum $ map tovalue xs

tovalue :: Char -> Int
tovalue c
  | charcode >= ca = charcode - ca + 1
  | otherwise = charcode - cA + 27
  where
    charcode = ord c
    ca = ord 'a'
    cA = ord 'A'

compareCompartments :: (String, String) -> [Char]
compareCompartments (xs, ys) = take 1 $ intersect xs ys
