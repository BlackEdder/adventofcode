module Main where

import Test.Tasty
import Test.Tasty.HUnit

main = do
  defaultMain tests
  print "Done"

myFiles = ["{smp}", "{smp2}", "{ipt}"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let lns = lines contents
        1 @?= 2,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        1 @?= 2,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        1 @?= 2,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        1 @?= 2
    ]
