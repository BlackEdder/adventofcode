module Main where

import Data.Sequences (dropEnd)
import Common (extractNumbers2)
import Data.List (foldl', iterate', scanl', sortBy)
import qualified Data.Set as Set
import Test.Tasty
import Test.Tasty.HUnit

type Lava = (Int, Int, Int)

readLine ln = (x, y, z)
  where
    [x, y, z] = extractNumbers2 ln

isNeighbour (x0, y0, z0) (x1, y1, z1) = dx + dy + dz == 1
  where
    dx = abs (x0 - x1)
    dy = abs (y0 - y1)
    dz = abs (z0 - z1)

getNeighbours blocks y = go (Set.delete y blocks) y
  where
    go blocks y = xs''' 
      where
        (x0, y0, z0) = y
        xs' = Set.filter (\(x1, _, _) -> abs (x0 - x1) < 2) blocks
        xs'' = Set.filter (isNeighbour (x0, y0, z0)) xs'
        (l, c) = Set.foldl' (\(s1, s2) x -> let (x1, x2) = go s1 x in (x1, Set.union x2 s2)) (Set.difference blocks xs'', xs'') xs''
        xs''' = (l, Set.insert y c)

collectClusters blocks = go [c] l
  where
    (l, c) = getNeighbours blocks (Set.elemAt 0 blocks)
    go xs left
      | null left = xs
      | otherwise =
        let (l', c') = getNeighbours left (Set.elemAt 0 left)
         in go (c' : xs) l'

neighbours xs y = go 6 xs y
  where
    go i [] y = i
    go 0 xs y = 0
    go i ((x0, y0, z0) : xs) (x1, y1, z1)
      | (x0 - x1) >= 2 = i
      | (x1 - x0) >= 2 = go i xs (x1, y1, z1)
      | isNeighbour (x0, y0, z0) (x1, y1, z1) = go (i - 1) xs (x1, y1, z1)
      | otherwise = go i xs (x1, y1, z1)

part1 blocks = go 0 blocks blocks
  where
    go i xs [] = i
    go i xs ((x0, y0, z0) : blcks) = go (i + i') xs' blcks
      where
        xs' = dropWhile (\(x1, _, _) -> x0 - x1 >= 2) xs
        i' = neighbours xs' (x0, y0, z0)

part2 blocks ns = go [] blocks ns
  where
    go is xs [] = is
    go is xs ((x0, y0, z0) : blcks) = go (i' : is) xs' blcks
      where
        xs' = dropWhile (\(x1, _, _) -> x0 - x1 >= 2) xs
        i' = 6 - neighbours xs' (x0, y0, z0)

negativeSpace blocks = xyz'
  where
    (x0, y0, z0) = foldl' (\(x0, y0, z0) (x1, y1, z1) -> (min x0 x1, min y0 y1, min z0 z1)) (head blocks) blocks
    (x1, y1, z1) = foldl' (\(x0, y0, z0) (x1, y1, z1) -> (max x0 x1, max y0 y1, max z0 z1)) (head blocks) blocks
    xs = [x0 .. x1]
    ys = [y0 .. y1]
    zs = [z0 .. z1]
    xyz = concatMap (\z -> concatMap (\y -> map (\x -> (x, y, z)) xs) ys) zs
    xyz' = filter (\p -> null $ filter (\b -> b == p) blocks) xyz

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day18.txt", "sample_day18_2.txt", "input_day18.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let blocks = sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ map readLine $ lines contents
        let sol = part1 blocks
        sol @?= 64,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let blocks = sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ map readLine $ lines contents
        let sol = part1 blocks
        sol @?= 4580,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let blocks = sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ map readLine $ lines contents
        let sol = part1 blocks
        let ns = sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ negativeSpace blocks
        let sol' = part2 blocks ns
        let sol'' = sol - 6 * (length $ filter (\x -> x == 6) sol')
        let blocks' = Set.fromList blocks
        let ns' = Set.fromList ns
        let clusters = collectClusters ns'
        let blocks' = Set.fromList blocks
        let ns' = Set.fromList ns
        let (left, cluster) = getNeighbours ns' (head ns)
        let clusters = collectClusters ns'
        let x = map (sum . part2 blocks) $ map (\x -> sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ Set.toList x) clusters
        let x' = sum $ dropEnd 1 x
        --2600 is too low
        -- Need to find clusters, discard the ones on the edge (should be 1 on the outside)
        sol - x' @?= 58,
      --1 @?= 58,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let blocks = sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ map readLine $ lines contents
        let sol = part1 blocks
        let ns = sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ negativeSpace blocks
        let sol' = part2 blocks ns
        -- 4280 is too high
        let sol'' = sol - 6 * (length $ filter (\x -> x == 6) sol')

        let blocks' = Set.fromList blocks
        let ns' = Set.fromList ns
        let (left, cluster) = getNeighbours ns' (head ns)
        let clusters = collectClusters ns'
        let (x0, y0, z0) = foldl' (\(x0, y0, z0) (x1, y1, z1) -> (min x0 x1, min y0 y1, min z0 z1)) (head blocks) blocks
        let (x1, y1, z1) = foldl' (\(x0, y0, z0) (x1, y1, z1) -> (max x0 x1, max y0 y1, max z0 z1)) (head blocks) blocks
        -- Filter edges
        let clusters' =  filter (\s -> null $ Set.filter (\(x2, y2, z2) -> x2 == x0 || x2 == x1 || y2 == y0 || y2 == y1 || z2 == z0 || z2 == z1) s) clusters
     

        let x = map (sum . part2 blocks) $ map (\x -> sortBy (\(x0, _, _) (x1, _, _) -> compare x0 x1) $ Set.toList x) clusters'
        --2600 is too low
        -- Need to find clusters, discard the ones on the edge (should be 1 on the outside)
        sol - sum x @?= 2610
    ]
