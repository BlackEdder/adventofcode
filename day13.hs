module Main where

import Common (readNumber)
import Data.Char (isDigit)
import Data.List (foldl', sortBy)
import Data.List.Split (splitWhen)
import Test.Tasty
import Test.Tasty.HUnit

encase :: String -> String
encase str
  | isDigit $ head str =
    let (dg, xs) = span isDigit str
     in '[' : dg ++ ']' : xs
  | otherwise = error "encase"

-- Technically they are equal here. Luckily did not have to consider this
checkOrder [[], []] = True
checkOrder [[], ys] = True
checkOrder [xs, []] = False
checkOrder [',' : xs, ',' : ys] = checkOrder [xs, ys]
checkOrder ['[' : xs, '[' : ys] = checkOrder [xs, ys]
checkOrder [']' : xs, ']' : ys] = checkOrder [xs, ys]
-- List is shorter
checkOrder [']' : xs, y : ys] = True
-- List is longer
checkOrder [x : xs, ']' : ys] = False
checkOrder ['[' : xs, y : ys]
  | isDigit y = checkOrder ['[' : xs, encase (y : ys)]
  | otherwise = error "Not sure what the rule here would be"
checkOrder [x : xs, '[' : ys]
  | isDigit x = checkOrder [encase (x : xs), '[' : ys]
  | otherwise = error "Not sure what the rule here would be"
checkOrder [xs, ys]
  | isDigit (head xs) && isDigit (head ys) =
    let (a, xss) = readNumber xs
        (b, yss) = readNumber ys
     in go a b xss yss
  | otherwise = error $ "parsing: " ++ show xs ++ " vs " ++ show ys
  where
    go a b xss yss
      | b < a = False
      | b == a = checkOrder [xss, yss]
      | otherwise = True

scoreOrder :: String -> String -> Ordering
scoreOrder xs ys
  | checkOrder [xs, ys] = LT
  | otherwise = GT 

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day13.txt", "sample_day13_2.txt", "input_day13.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "FunctionTests" $ do
        readNumber "1,ab" @?= (1, ",ab")
        readNumber "10,ab" @?= (10, ",ab")
        readNumber "10," @?= (10, ","),
      testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let pairs = splitWhen null $ lines contents
        let pair = head pairs
        let sc = foldl' (\acc (x, y) -> acc + x) 0 $ filter snd $ zip [1 ..] $ map checkOrder pairs
        sc @?= 13,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let pairs = splitWhen null $ lines contents
        let pair = head pairs
        let sc = foldl' (\acc (x, y) -> acc + x) 0 $ filter snd $ zip [1 ..] $ map checkOrder pairs
        sc @?= 5185,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let msgs = filter (not . null) $ lines contents ++ ["[[2]]", "[[6]]"]
        let ordr = sortBy scoreOrder msgs
        let v = product $ map fst $ filter (\(_, x) -> x == "[[2]]" || x == "[[6]]") $ zip [1 ..] ordr
        v @?= 140,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let msgs = filter (not . null) $ lines contents ++ ["[[2]]", "[[6]]"]
        let ordr = sortBy scoreOrder msgs
        let v = product $ map fst $ filter (\(_, x) -> x == "[[2]]" || x == "[[6]]") $ zip [1 ..] ordr
        v @?= 23751
    ]
