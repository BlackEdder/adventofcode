module Main where

import Common (readInt)
import Data.List (scanl')
import Data.Sequences (dropEnd)
import Data.List.Split (chunksOf)
import Test.Tasty
import Test.Tasty.HUnit

data Instruction = Noop | Addx Int deriving (Show)

readInstruction :: String -> Instruction
readInstruction str
  | str == "noop" = Noop
  | otherwise = let ws = words str in Addx (readInt (last ws))

executeInstruction :: Int -> Instruction -> [Int]
executeInstruction x Noop = [x]
executeInstruction x (Addx dx) = [x, x + dx]

registers :: [String] -> [Int]
-- Drop the final one
registers lns = dropEnd 1 xs
  where
    instructions = map readInstruction lns
    xs = concat $ scanl' (executeInstruction . last) [1] instructions

toScreen :: [Int] -> String
toScreen xs = map (\(c, x) -> if abs (x - c) <= 1 then '#' else '.') zs
  where
    zs = zip (map (`mod` 40) [0 ..]) xs

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day10.txt", "sample_day10_2.txt", "input_day10.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let xs = registers $ lines contents
        xs !! 5 @?= 16 - 11
        xs !! 19 @?= 21
        xs !! 59 @?= 19
        -- TODO: Custom function that drops the previous value and then takes the 40th value
        let cycles = scanl' (\prev i -> prev + 40) 20 [0 .. 4]
        let sol = foldl (\acc cyc -> acc + cyc * xs !! (cyc - 1)) 0 cycles
        sol @?= 13140,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let xs = registers $ lines contents
        let cycles = scanl' (\prev i -> prev + 40) 20 [0 .. 4]
        let sol = foldl (\acc cyc -> acc + cyc * xs !! (cyc - 1)) 0 cycles
        sol @?= 16060,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let crt = toScreen $ registers $ lines contents
        crt @?= "##..##..##..##..##..##..##..##..##..##..###...###...###...###...###...###...###.####....####....####....####....####....#####.....#####.....#####.....#####.....######......######......######......###########.......#######.......#######.....",
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let crt = toScreen $ registers $ lines contents
        crt @?= "###...##...##..####.#..#.#....#..#.####.#..#.#..#.#..#.#....#.#..#....#..#.#....###..#..#.#....###..##...#....####.###..#..#.####.#....#....#.#..#....#..#.#....#..#.#..#.#..#.#....#.#..#....#..#.#....###..#..#..##..####.#..#.####.#..#.#...."
        print ""
        print ""
        let crtLines = chunksOf 40 crt
        mapM_ putStrLn crtLines
    ]
