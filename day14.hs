module Main where

import Common (extractNumbers, nubOrd)
import Data.List (foldl', iterate', maximumBy)
import qualified Grid
import Test.Tasty
import Test.Tasty.HUnit

data Material = Empty | Rock | Sand deriving (Show)

type Direction = (Int, Int)

move :: Grid.Position -> Direction -> Grid.Position
move (x, y) (a, b) = (x + a, y + b)

interpolate (x0, y0) (x1, y1) = go [(x0, y0)] (dx, dy) (x1, y1)
  where
    (dx, dy) = (signum $ x1 - x0, signum $ y1 - y0)
    go (x : xs) (dx, dy) to
      | dx /= 0 && dy /= 0 = error "Diagonal is not supported"
      | nxt == to = nxt : x : xs
      | otherwise = go (nxt : x : xs) (dx, dy) to
      where
        nxt = move (dx, dy) x

interpolateRocks xs = rcks
  where
    zp = zip xs (drop 1 xs)
    rcks = concatMap (uncurry interpolate) zp

xoffset = 250

xmax = 500

readLine str = go [] nos
  where
    nos = extractNumbers str
    go xs [] = xs
    go xs (x : y : xys)
      | x < xoffset || x > xoffset + xmax || y < 0 || y > 200 = error "Size not supported"
      | otherwise = go ((x - xoffset, y) : xs) xys

empty Empty = True
empty _ = False

fallingSand maxY grd = go (500 - xoffset, 0) grd
  where
    go :: Grid.Position -> Grid.Grid Material -> (Int, Grid.Grid Material)
    go (x, y) grd
      | y == maxY = (-1, grd)
      | empty pos1 = go (x, y + 1) grd
      | empty pos2 = go (x - 1, y + 1) grd
      | empty pos3 = go (x + 1, y + 1) grd
      | otherwise = if y == 0 then (-1, grd) else (1, Grid.setPosition (x, y) Sand grd)
      where
        pos1 = Grid.atPosition grd (x, y + 1)
        pos2 = Grid.atPosition grd (x - 1, y + 1)
        pos3 = Grid.atPosition grd (x + 1, y + 1)

addRocks rocks grd = foldl' (\acc rock -> Grid.setPosition rock Rock acc) grd rocks

parseInput contents = (maxY, ngrd)
  where
    grd = Grid.const xmax 200 Empty
    lns = lines contents
    rockLines = map readLine lns
    rocks = nubOrd $ concatMap interpolateRocks rockLines
    (_, maxY) = maximumBy (\(_, y0) (_, y1) -> compare y0 y1) rocks
    ngrd = addRocks rocks grd

main = do
  defaultMain tests
  print "Done"

myFiles = ["sample_day14.txt", "sample_day14_2.txt", "input_day14.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        interpolate (498, 4) (498, 6) @?= [(498, 6), (498, 5), (498, 4)]
        interpolate (498, 4) (496, 4) @?= [(496, 4), (497, 4), (498, 4)]
        1 @?= 1,
      testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let (_, grd) = parseInput contents
        let xs = iterate' (\(x, grd) -> fallingSand 199 grd) (1, grd)
        let l = length $ takeWhile (\(x, grd) -> x > 0) xs
        l - 1 @?= 24,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let (_, grd) = parseInput contents
        let xs = iterate' (\(x, grd) -> fallingSand 199 grd) (1, grd)
        let l = length $ takeWhile (\(x, grd) -> x > 0) xs
        l - 1 @?= 1003,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let (maxy, grd) = parseInput contents
        let floor = interpolate (0, maxy + 2) (xmax - 1, maxy + 2)
        let ngrd = addRocks floor grd
        let xs = iterate' (\(x, grd) -> fallingSand 800 grd) (1, ngrd)
        let l = length $ takeWhile (\(x, grd) -> x > 0) xs
        l @?= 93,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let (maxy, grd) = parseInput contents
        let floor = interpolate (0, maxy + 2) (xmax - 1, maxy + 2)
        let ngrd = addRocks floor grd
        let xs = iterate' (\(x, grd) -> fallingSand 800 grd) (1, ngrd)
        let l = length $ takeWhile (\(x, grd) -> x > 0) xs
        l @?= 25771
    ]
