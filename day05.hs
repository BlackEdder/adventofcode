import Data.Char (ord)
import Data.List
import Data.List.Extra (trim)
import Data.List.Split

-- Label
type Crate = Char

-- Amount, from, to
type Move = (Int, Int, Int)

-- [[Char]]
type Stacks = [[Crate]]

main = do
  contents <- readFile "input_day05.txt"
  let lns = lines contents
  let [h, v] = splitOn [""] lns
  let header = take (length h - 1) h 
  -- Read stacks
  let cols = transpose $ map extractCrateLabels header
  let stacks = map trim cols :: Stacks
  -- Read the moves 
  let moves = map readMove v 
  let new_stacks = foldl moveCrates stacks moves
  -- Part 1: JCMHLVGMG
  print $ map head new_stacks
  -- Part 2: LVMRWSSPZ 
  let new_stacks2 = foldl moveCrates9001 stacks moves
  print $ map head new_stacks2

moveCrates :: Stacks -> Move -> Stacks
moveCrates stacks move = putCrates move $ takeCrates move stacks

moveCrates9001 :: Stacks -> Move -> Stacks
moveCrates9001 stacks move = putCrates move $ takeCrates9001 move stacks

-- n, col
takeCrates :: Move -> Stacks -> (Stacks, [Crate])
takeCrates (n, col, _) stacks = (xs ++ [y] ++ zs, crates)
    where (xs, current_y, zs) = breakAround col stacks
          crates = reverse $ take n current_y
          y = drop n current_y

takeCrates9001 :: Move -> Stacks -> (Stacks, [Crate])
takeCrates9001 (n, col, _) stacks = (xs ++ [y] ++ zs, crates)
    where (xs, current_y, zs) = breakAround col stacks
          crates = take n current_y
          y = drop n current_y

putCrates :: Move -> (Stacks, [Crate]) -> Stacks
putCrates (_, _, col) (stacks, crates) = xs ++ [y] ++ zs
    where (xs, current_y, zs) = breakAround col stacks
          y = crates ++ current_y

breakAround :: Int -> [a] -> ([a], a, [a]) 
breakAround i ys = (x, y, z) 
    where x = take (i - 1) ys
          y = ys !! (i - 1)
          z = drop i ys

readMove :: String -> Move
readMove str | length nbrs == 3 = (read x :: Int, read y :: Int, read z :: Int)
             | otherwise = error $ "Parse error: " ++ str
  where
    nbrs = filter (\x -> ord (head x) >= ord '0' && ord (head x) <= ord '9') $ words str
    x:y:z:_ = nbrs

extractCrateLabels :: String -> String
extractCrateLabels [] = []
extractCrateLabels str = crate : extractCrateLabels (drop 4 str)
  where
    crate = last $ take 2 str
