import Control.Monad
import Data.Char (ord)
import Data.List
import System.IO

main = do
  contents <- readFile "input_day02.txt"
  --map readInt . wrds
  let games = map readLine . lines $ contents
  let ts = sum $ map typeScore games
  let gs = sum $ map gameScore games
  -- Part 1
  print $ gs + ts
  -- Part 2
  let games2 = map gameConvert games
  let ts2 = sum $ map typeScore games2
  let gs2 = sum $ map gameScore games2
  print $ gs2 + ts2

gameConvert :: [Int] -> [Int]
gameConvert [a, b]
  | b == 2 = a : [a]
  | b == 1 && a == 1 = a : [3]
  | b == 1 = a : [a - 1]
  | b == 3 = a : [mod a 3 + 1]

readLine :: [Char] -> [Int]
readLine [a, _, c] = (ord a - 64) : [ord c - 87]
readLine _ = []

typeScore :: [Int] -> Int
typeScore [a, b] = b

gameScore :: [Int] -> Int
gameScore [a, b]
  | a == b = 3
  | a == 1 && b == 3 = 0
  | a == 3 && b == 1 = 6
  | b > a = 6
  | b < a = 0
