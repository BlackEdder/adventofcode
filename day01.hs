import System.IO  
import Control.Monad
import Data.List (sort)
import Data.List.Split (splitWhen)

main = do 
    contents <- readFile "input_day01.txt"
    let elves = splitWhen null $ lines contents
    let calories = map (sum . map readInt) elves
    print $ sum $ take 3 $ reverse $ sort calories 

readInt :: String -> Int
readInt = read

