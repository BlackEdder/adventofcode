module Main where

import Data.List (minimumBy)

data Entry = File String Int | Dir String deriving (Show)

data Command = Cd String | Ls [Entry] deriving (Show)

--type Position = (depth, name, size)
type Position = (Int, String, Int)

main = do
  contents <- readFile "input_day07.txt"
  let lns = lines contents
  let cmds = parseCommands lns
  -- tail, because I assume we start at the root (cd "/")
  let ps = executeCommands $ tail cmds
  let dirs = recursiveSize ps 
  -- part 1: 1367870 
  print $ foldl (\acc (_, _, s) -> acc + s) 0 $ filter (\(_, _, s) -> s < 100000) dirs
  let (_, _, current_size) = head dirs
  let needed = 30000000 - (70000000 - current_size)
  let (_, _, size) = minimumBy (\(_,_,a) (_,_,b) -> compare a b) $ filter (\(_, _, s) -> s > needed) dirs
  -- part 2: 549173 
  print size

-- TODO: We do loads of extra work here, because we don't store intermediate results
recursiveSize :: [Position] -> [Position]
recursiveSize = go []
    where
        go :: [Position] -> [Position] -> [Position]
        go pos [] = reverse pos
        go newps ((depth, nm, size):pos) = go ((depth, nm, size + totalsize):newps) pos
            where lower = takeWhile (\(d, _, _) -> d > depth) pos
                  totalsize = foldl (\acc (_, _, s) -> acc + s) 0 lower

executeCommands :: [Command] -> [Position]
executeCommands cmds = reverse $ filter (\(_, nm, _) -> nm /= "") $ go [(0, "/", 0)] cmds
--executeCommands cmds = reverse $ go [(0, "/", 0)] cmds
    where
        go :: [Position] -> [Command] -> [Position]
        go pos [] = pos
        go ((d, p1, p2):pos) ((Cd ".."):cmds) = go ((d - 1, "", 0):(d, p1, p2):pos) cmds
        go ((d, p1, p2):pos) ((Cd nm):cmds) = go ((d + 1, nm, 0):(d, p1, p2):pos) cmds
        go ((d, nm, s):pos) ((Ls es):cmds) = go ((d, nm, s + sizeDir es):pos) cmds


sizeDir :: [Entry] -> Int
sizeDir = foldl (\acc x -> acc + sizeEntry x) 0

sizeEntry :: Entry -> Int
sizeEntry (Dir _) = 0
sizeEntry (File _ s) = s

parseCommands :: [String] -> [Command]
parseCommands = go []
  where
    go :: [Command] -> [String] -> [Command]
    go cmds [] = cmds
    go cmds (x : xs)
      | isCommand x = go (cmds ++ [cmd]) ys
      | otherwise = error "Parse error"
      where
        results = takeWhile (not . isCommand) xs
        ys = drop (length results) xs
        cmd = parseCommand (map parseEntry results) x

parseCommand :: [Entry] -> String -> Command
parseCommand results str
  | (wrds !! 1) == "cd" = Cd (wrds !! 2)
  | (wrds !! 1) == "ls" = Ls results
  | otherwise = error "Parse error"
  where
    wrds = words str

parseEntry :: String -> Entry
parseEntry str
  | s0 == "dir" = Dir s1
  | otherwise = File s1 $ read s0
  where
    [s0, s1] = words str

isCommand :: String -> Bool
isCommand str = head str == '$'
