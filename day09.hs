module Main where

import Common (nubOrd)
import Data.List (foldl', scanl')
import Test.Tasty
import Test.Tasty.HUnit

type Direction = (Int, Int)

type Move = (Direction, Int)

type Position = (Int, Int)

type Head = Position

type Tail = Position

type Trail = [Position]

touching :: Tail -> Head -> Bool
touching (tx, ty) (hx, hy) = abs (tx - hx) <= 1 && abs (ty - hy) <= 1

nextTailStep :: Tail -> Head -> Tail
nextTailStep t h
  | touching t h = t
  | otherwise = (tx + signum dx, ty + signum dy)
  where
    (tx, ty) = t
    (hx, hy) = h
    (dx, dy) = (hx - tx, hy - ty)

readLine :: String -> Move
readLine (dir : _ : xs)
  | dir == 'R' = ((1, 0), n)
  | dir == 'L' = ((-1, 0), n)
  | dir == 'U' = ((0, 1), n)
  | dir == 'D' = ((0, -1), n)
  | otherwise = error "Parsing moves"
  where
    n = read xs :: Int

moveH :: Position -> Move -> Trail
moveH h (_, 0) = [h] 
moveH (x, y) ((dx, dy), n) = map (\i -> (x + i*dx, y + i*dy)) [1..n]
--moveH h ((dx, dy), n) = scanl (\(x, y) _ -> (x + dx, y + dy)) h [1..(n)]

applyMoves :: Position -> [Move] -> Trail
applyMoves h ms = concat $ scanl' (moveH . last) [h] ms

main = do
  defaultMain tests
  print "Done"

part1 :: [String] -> (Trail, Trail)
part1 lns = (trail, tailTrail)
  where
    moves = map readLine lns
    ropeH = (0, 0)
    ropeT = (0, 0)
    trail = applyMoves ropeH moves
    -- let tailTrail = foldl (\acc tr -> nextTailStep)
    tailTrail = scanl' nextTailStep ropeT trail

myFiles = ["sample_day09.txt", "sample_day09_2.txt", "input_day09.txt"]

tests =
  testGroup
    "Tests"
    [ testCase "SampleTest" $ do
        contents <- readFile $ head myFiles
        let lns = lines contents
        let (ht, rt) = part1 lns
        let n = length $ nubOrd rt
        last ht @?= (2, 2)
        n @?= 13,
      testCase "FullTest" $ do
        contents <- readFile $ last myFiles
        let lns = lines contents
        let (ht, rt) = part1 lns
        let n = length $ nubOrd rt
        n @?= 6197,
      testCase "SampleTestPart2" $ do
        contents <- readFile $ myFiles !! 1
        let lns = lines contents
        let (ht, rt) = part1 lns
        -- follow the trail 8 more times
        let lastTrail = foldl' (\acc x -> scanl' nextTailStep (0, 0) acc) rt [0 .. 7]
        let n = length $ nubOrd lastTrail
        n @?= 36,
      testCase "FullTestPart2" $ do
        contents <- readFile $ last myFiles
        let lns = lines contents
        let (ht, rt) = part1 lns
        let lastTrail = foldl' (\acc x -> scanl' nextTailStep (0, 0) acc) rt [0 .. 7]
        let n = length $ nubOrd lastTrail
        n @?= 2562
    ]
