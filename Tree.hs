module Tree where

import Data.List (break)

type Name = String

type Data = Int

data FSItem = File Name Data | Folder Name [FSItem] deriving (Show)

data FSCrumb = FSCrumb Name [FSItem] [FSItem] deriving (Show)

type FSZipper = (FSItem, [FSCrumb])

fsUp :: FSZipper -> FSZipper
fsUp (item, FSCrumb name ls rs : bs) = (Folder name (ls ++ [item] ++ rs), bs)

toRoot :: Name -> FSZipper -> FSZipper
toRoot _ (t, []) = (t, [])
-- We can stop following the zipper as soon as we reach the root folder!
toRoot nm (Folder name xs, z)
  | name == nm = (Folder nm xs, [])
  | otherwise = fsUp (Folder name xs, z)
toRoot _ z = fsUp z

fsTo :: Name -> FSZipper -> FSZipper
fsTo name (Folder folderName items, bs) =
  let (ls, item : rs) = break (nameIs name) items
   in (item, FSCrumb folderName ls rs : bs)

nameIs :: Name -> FSItem -> Bool
nameIs name (Folder folderName _) = name == folderName
nameIs name (File fileName _) = name == fileName

fsRename :: Name -> FSZipper -> FSZipper
fsRename newName (Folder name items, bs) = (Folder newName items, bs)
fsRename newName (File name dat, bs) = (File newName dat, bs)

fsNewItem :: FSItem -> FSZipper -> FSZipper
fsNewItem item (Folder folderName items, bs) =
  (Folder folderName (item : items), bs)
