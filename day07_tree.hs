module Main where

import Data.List (minimumBy)

import Tree

data Command = Cd String | Ls [FSItem] deriving (Show)

main = do
  contents <- readFile "input_day07.txt"
  let lns = lines contents
  let cmds = parseCommands lns
  -- tail, because I assume we start at the root (cd "/")
  let myDisk = toRoot "root" $ executeCommands (Folder "root" []) $ tail cmds
  let items = itemsSize $ fst myDisk
  let smallest = filter (\(_, s, x) -> x == 0 && s < 100000) items
  -- part 1: 1367870 
  print $ foldl (\acc (_, s, 0) -> acc + s) 0 smallest
  let (_, current_size, _) = head items
  let needed = 30000000 - (70000000 - current_size)
  let (_, size, _) = minimumBy (\(_,a,_) (_,b,_) -> compare a b) $ filter (\(_, s, _) -> s > needed) items 
  -- part 2: 549173 
  print size

itemsSize :: FSItem -> [(String, Int, Int)]
itemsSize = go [("root", 0, 0)]
  where
    go xs (File nm s) = (nm, 0, s):xs
    go xs (Folder nm ss) = (nm, size, 0):subdir ++ xs
      where
        -- Collect all subdir items
        subdir = foldl (\acc s -> acc ++ go [] s) [] ss
        -- Size of all the folders in the subdir
        size = foldl (\acc (_, _, s) -> acc + s) 0 subdir

executeCommands :: FSItem -> [Command] -> FSZipper
executeCommands item = go (item, [])
  where
    go :: FSZipper -> [Command] -> FSZipper
    go zip [] = zip
    go zip ((Cd "..") : xs) = go (fsUp zip) xs
    go zip ((Cd nm) : xs) = go (fsTo nm zip) xs
    go zip ((Ls entries) : xs) = go res xs
      where
        res = foldl (flip fsNewItem) zip entries

parseCommands :: [String] -> [Command]
parseCommands = go []
  where
    go :: [Command] -> [String] -> [Command]
    go cmds [] = cmds
    go cmds (x : xs)
      | isCommand x = go (cmds ++ [cmd]) ys
      | otherwise = error "Parse error"
      where
        results = takeWhile (not . isCommand) xs
        ys = drop (length results) xs
        cmd = parseCommand (map parseEntry results) x

parseCommand :: [FSItem] -> String -> Command
parseCommand results str
  | (wrds !! 1) == "cd" = Cd (wrds !! 2)
  | (wrds !! 1) == "ls" = Ls results
  | otherwise = error "Parse error"
  where
    wrds = words str

parseEntry :: String -> FSItem
parseEntry str
  | s0 == "dir" = Folder s1 []
  | otherwise = File s1 $ read s0
  where
    [s0, s1] = words str

isCommand :: String -> Bool
isCommand str = head str == '$'
