module Main where

import Common (mapWIndex, nubOrd)
import Data.List (transpose)

type Height = Int

type Position = (Int, Int)

type Tree = (Height, Position)

type Trees = [Tree]

hTree :: Tree -> Height
hTree (h, _) = h

pTree :: Tree -> Position
pTree (_, p) = p

main = do
  contents <- readFile "input_day08.txt"
  let lns = lines contents

  let rows = mapWIndex (uncurry readLine) lns
  let cols = transpose rows
  let m1 = concatMap visibleTrees cols
  let m2 = concatMap (visibleTrees . reverse) cols
  let m3 = concatMap visibleTrees rows
  let m4 = concatMap (visibleTrees . reverse) rows
  let potentialTrees = nubOrd (m1 ++ m2 ++ m3 ++ m4)
  -- part 1: 1546
  print $ length potentialTrees
  -- part 2: 519064
  print $ maximum $ map (scoreTree rows cols) potentialTrees

countVis :: Tree -> Trees -> Int
countVis t ts = go 0 t ts
  where
    go i t [] = i
    go i t (x : xs)
      | hTree t > hTree x = go (i + 1) t xs
      | hTree t <= hTree x = i + 1
      | otherwise = error "countVis"

scoreTree :: [Trees] -> [Trees] -> Tree -> Int
scoreTree rs cs t
  -- Edges will have a score of zero
  | i == 0 || j == 0 = 0
  | i == length rs - 1 || j == length cs - 1 = 0
  | otherwise = product ss
  where
    (i, j) = pTree t
    (left, right) = splitAt j (rs !! i)
    (up, down) = splitAt i (cs !! j)
    ss = [countVis t $ reverse left, countVis t $ tail right, countVis t $ reverse up, countVis t $ tail down]

readInt :: Char -> Int
readInt x = read [x]

readLine :: Int -> [Char] -> Trees
readLine i = mapWIndex (\(j, x) -> (readInt x, (i, j)))

visibleTrees :: Trees -> Trees
visibleTrees ys = go (-1) [] ys
  where
    --zs = takeWhile (\x -> hTree x /= 9) ys
    go :: Height -> Trees -> Trees -> Trees
    -- If tree was maximum height, we can break/stop
    go 9 xs ys = xs
    go mx [] (y : ys) = go (hTree y) [y] ys
    go mx xs [] = xs
    go mx xs (y : ys)
      | hTree y > mx = go (hTree y) (xs ++ [y]) ys
      | otherwise = go mx xs ys
